set -e
set -xif [ "$RUN_MIGRATIONS" ]; then
  echo "RUNNING MIGRATIONS";
  yarn run typeorm:migration:run
fiecho "START SERVER";
yarn run start:prod