import { Injectable, Post } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ItemEntity } from '../model/item.entity';
import { Repository } from 'typeorm';
import { ItemDTO } from './item.dto';
import { User } from 'src/user.decorator';

@Injectable()
export class ItemService {
    constructor(@InjectRepository(ItemEntity) private readonly repo: Repository<ItemEntity>) { }

    public async getAll(): Promise<ItemDTO[]> {
        return await this.repo.find()
            .then(items => items.map(e => ItemDTO.fromEntity(e)));
    }
}